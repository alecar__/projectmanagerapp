import { CommentsProvider } from './../../providers/comments/comments';
import { TasksProvider } from './../../providers/tasks/tasks';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
  task:any;
  comments:any = [];
  url:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public taskPvr:TasksProvider,public commentPvr:CommentsProvider) {
    this.task = this.navParams.get("task");
    let token = localStorage.getItem("token");
    this.getComments(token,this.task.id);
    this.url = localStorage.getItem("url");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  getComments(token:string,task_id:number){
    this.taskPvr.comments(token,task_id).subscribe(
      data=> {
        let datajson = data.json();
        this.comments = [];
        if(!datajson.errors && datajson.data != null){
          if(datajson.many){
            this.comments = datajson.data;
          }else{
            this.comments.push(datajson.data);
          }
        }
        console.log(datajson);
      },
      error => {
        this.closeSession(error.json());
      }
    )
  }

  newComment(comment_id:number){
    let input:any;
    let comment:string;
    let token = localStorage.getItem("token");
    if(comment_id != null){
      input = document.getElementById('newcomment'+comment_id);
    }else{
      input = document.getElementById('newcomment');
    }
    comment = input.value;
    input.value = "";

    this.commentPvr.newComment(token,comment,comment_id,this.task.id,1).subscribe(
      data => {
        let datajson = data.json();
        console.log(datajson);
        if(!datajson.errors){
          datajson.user = JSON.parse(localStorage.getItem("user"));
          this.comments.push(datajson);
        }
      },
      error => {
        this.closeSession(error.json());
      }
    )

  }

  closeSession(error:any){
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.navCtrl.setRoot(HomePage);
  }

}
