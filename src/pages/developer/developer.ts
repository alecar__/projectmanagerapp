import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TasksProvider } from '../../providers/tasks/tasks';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-developer',
  templateUrl: 'developer.html',
})
export class DeveloperPage {
  task:any;
  project:any;
  developers:any = [];
  url:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public taskPvr: TasksProvider) {
    this.task = this.navParams.get("task");
    this.project = this.navParams.get("project");
    let token = localStorage.getItem("token");
    this.getDevelopers(token,this.task.id);
    this.url = localStorage.getItem("url");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeveloperPage');
  }

  getDevelopers(token:string,task:number){
    this.taskPvr.developers(token,task).subscribe(
      data=> {
        let datajson = data.json();
        this.developers = [];
        if(!datajson.errors && datajson.data != null){
          if(datajson.many){
            this.developers = datajson.data;
          }else{
            this.developers.push(datajson.data);
          }
        }
        console.log(datajson);
      },
      error => {
        this.closeSession(error.json());
      }
    )
  }

  parseTimeToString(minutes){
    var seconds = Math.floor(minutes*60);
    var numdays = Math.floor(seconds / 86400);
    var numhours = Math.floor((seconds % 86400) / 3600);
    var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);

    if(numdays == 0){
      if(numhours == 0){
        return numminutes + " minutos ";
      }else{
        return numhours + " horas " + numminutes + " minutos ";
      }
    }else{
      return numdays + " dias " + numhours + " horas " + numminutes + " minutos ";
    }
  }

  closeSession(error:any){
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.navCtrl.setRoot(HomePage);
  }
}
