import { SessionProvider } from './../../providers/session/session';
import { ProjectsProvider } from './../../providers/projects/projects';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskPage } from '../task/task';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  user:any;

  projects:any = [];
  url:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public projectsPvr: ProjectsProvider,public sessionPvr: SessionProvider) {
    let token = localStorage.getItem("token");
    this.getProjects(token);

    this.getCurrentUser();
    this.url = localStorage.getItem("url");
  }

  getProjects(token:string){
    this.projectsPvr.projects(token).subscribe(
      data=> {
        let datajson = data.json();
        if(!datajson.erros && datajson.data != null){
          if(datajson.many){
            this.projects = datajson.data;
          }else{
            this.projects.push(datajson.data);
          }
        }
      },
      error => {
        this.closeSession(error.json());
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }
  
  deleteProject(id:number){
    console.log(id);
  }

  goToTasks(project:any){
    console.log(project);
    this.navCtrl.push(TaskPage,{'project' : project});
  }

  getCurrentUser(){
    let token = localStorage.getItem("token");
    this.sessionPvr.currentuser(token).subscribe(
      data=>{
        let datajson = data.json();
        console.log(datajson);
        this.user = datajson;
        localStorage.setItem("user",JSON.stringify(datajson));
      }
    )
  }

  logOut(){
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.navCtrl.setRoot(HomePage);
  }

  closeSession(error:any){
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.navCtrl.setRoot(HomePage);
  }
}
