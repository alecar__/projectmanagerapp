import { TasksProvider } from './../../providers/tasks/tasks';
import { CommentPage } from './../comment/comment';
import { DeveloperPage } from './../developer/developer';
import { ProjectsProvider } from './../../providers/projects/projects';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MydataPage } from '../mydata/mydata';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {
  project:any;
  tasks:any;
  mydata:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public projectPvr: ProjectsProvider, public alertCtrl: AlertController,public taskPvr:TasksProvider) {
    this.project = this.navParams.get("project");
    let token = localStorage.getItem("token");
    this.getTasks(token,this.project.id);
  }

  getTasks(token:string,project:number){
    this.projectPvr.tasks(token,project).subscribe(
      data=> {
        let datajson = data.json();
        this.tasks = [];
        if(!datajson.errors && datajson.data != null){
          if(datajson.many){
            this.tasks = datajson.data;
          }else{
            this.tasks.push(datajson.data);
          }
        }
        console.log(datajson);
      },
      error => {
        this.closeSession(error.json());
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskPage');
  }

  goToDevelopers(task:any){
    this.navCtrl.push(DeveloperPage,{"task":task,"project":this.project});
  }
  
  goToComments(task:any){
    this.navCtrl.push(CommentPage,{"task":task})
  }

  goToData(task:any){
    this.navCtrl.push(MydataPage,{"task":task})
  }

  statusToString(status:number){
    if (status == 0){
      return "toDo";
    }else if(status == 1){
      return "inProgress";
    }else if(status == 2){
      return  "finished"
    }   
  }

  changeStatus(id:number,status:number,index_element:number){
    let alert = this.alertCtrl.create();
    alert.setTitle('Status');

    alert.addInput({
      type: 'radio',
      label: 'Todo',
      value: '0',
      checked: status == 0
    });

    alert.addInput({
      type: 'radio',
      label: 'In Progress',
      value: '1',
      checked: status == 1
    });

    alert.addInput({
      type: 'radio',
      label: 'Finished',
      value: '2',
      checked: status == 2
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Guardar',
      handler: data => {
        let token = localStorage.getItem("token");
        this.changeTask(token,id,data,index_element);
      }
    });
    alert.present();
  }

  changeTask(token:string,id:number,status:number,index_element:number){
    this.taskPvr.changestatus(token,id,status).subscribe(
      data => {
        let datajson = data.json();
        if(!datajson.errors){
          this.tasks[index_element]['status'] = datajson.status;
        }
      },
      error => {
        this.closeSession(error.json());
      }
    )
  }

  showMyData(id:number,index_element:number){
    let token = localStorage.getItem("token");
    let user = JSON.parse(localStorage.getItem("user"));
    this.taskPvr.gettaskuser(token,id,user.id).subscribe(
      data=>{
        console.log(data.json());
        let datajson = data.json();
        if (!datajson.errors) {
          this.mydata = datajson;
          let prompt = this.alertCtrl.create({
            title: 'Mis Datos',
            message: "Escriba la informacion solicitada.",
            inputs: [
              {
                name: 'tiempo',
                placeholder: 'Ingrese los minutos invertidos en esta tarea',
                value: this.mydata.total_time,
              },
              {
                name: 'progreso',
                placeholder: 'Ingrese el porcentaje en que se encuentra en la tarea',
                value: this.mydata.percentage,
              },
            ],
            buttons: [
              {
                text: 'Cancelar',
                handler: data => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Guardar',
                handler: data => {
                  console.log(data);
                  let time = parseInt(data.tiempo);
                  let progress = parseFloat(data.progreso);

                  if(time >= this.mydata.total_time && progress >= this.mydata.percentage){
                    let mydata = {
                      total_time: time,
                      percentage: progress
                    };
  
                    this.changeMyData(token,id,user.id,mydata);
                  }else{
                    let alert = this.alertCtrl.create({
                      title: 'Valores erroneos',
                      subTitle: 'El tiempo y el progreso no pueden ser menores que los ya almacenados.',
                      buttons: ['OK']
                    });
                    alert.present();
                  }
                }
              }
            ]
          });
          prompt.present();
        }
      },
      error=>{
        this.closeSession(error.json());
      }
    )
  }

  changeMyData(token:string,task:number,user:number,data:any){
    this.taskPvr.changemydata(token,task,user,data).subscribe(
      data=>{
        let datajson = data.json();
        if(!datajson.errors && datajson != null){
          this.mydata = datajson;
        }
      },
      error=>{
        this.closeSession(error.json());
      }
    )
  }

  closeSession(error:any){
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.navCtrl.setRoot(HomePage); 
  }

}
