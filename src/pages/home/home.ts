import { DashboardPage } from './../dashboard/dashboard';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { SessionProvider } from './../../providers/session/session';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  user:string = "";
  password:string = "";

  constructor(public navCtrl: NavController, private sessions:SessionProvider,public alertCtrl:AlertController) {
    
  }

  login(){
    this.sessions.login(this.user,this.password).subscribe(
      data=>{
        let datajson = data.json();
        this.saveDataInCache(datajson);
        this.navCtrl.setRoot(DashboardPage);
      },
      error => {
        let alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: 'El usuario o contraseña son erroneos!',
          buttons: ['OK']
        });
        alert.present();
      }
    )
  }

  /*currentuser(token:string){
    this.sessions.currentuser(token).subscribe(
      data=> {
        let datajson = data.json();
        if(datajson != null){
          localStorage.setItem("user",JSON.stringify(datajson.user));
        }
      }
    )
  }*/

  saveDataInCache(json:any){
    let token = json.token;
    localStorage.setItem("token", token);
  }

  


}
