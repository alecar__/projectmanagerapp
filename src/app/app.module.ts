import { MydataPage } from './../pages/mydata/mydata';
import { CommentPage } from './../pages/comment/comment';
import { DeveloperPage } from './../pages/developer/developer';
import { TaskPage } from './../pages/task/task';
import { DashboardPage } from './../pages/dashboard/dashboard';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SessionProvider } from '../providers/session/session';
import { HttpModule } from '@angular/http';
import { ProjectsProvider } from '../providers/projects/projects';
import { TasksProvider } from '../providers/tasks/tasks';
import { CommentsProvider } from '../providers/comments/comments';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DashboardPage,
    TaskPage,
    DeveloperPage,
    CommentPage,
    MydataPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DashboardPage,
    TaskPage,
    DeveloperPage,
    CommentPage,
    MydataPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SessionProvider,
    ProjectsProvider,
    TasksProvider,
    CommentsProvider
  ]
})
export class AppModule {}
