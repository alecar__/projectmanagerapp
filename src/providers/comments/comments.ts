import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';

@Injectable()
export class CommentsProvider {

  url:string;

  constructor(public http: Http) {
    this.url = localStorage.getItem("url")
  }

  newComment(token:string,comment:string,parent_comment:number,task:number,user:number){ 
    let url = this.url+"/comments/";
    let body;
    if(parent_comment != null){
      body = JSON.stringify({task: task, user: user,title: "comment",comment: comment,parent_comment: parent_comment});
    }else{
      body = JSON.stringify({task: task, user: user,title: "comment",comment: comment});
    }

    let headers = new Headers({'Content-Type': 'application/json','Authorization': 'JWT '+token});
    return this.http.post(url,body,{headers:headers});
  }
  
}
