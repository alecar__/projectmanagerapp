import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';

@Injectable()
export class ProjectsProvider {

  url:string;
  
  constructor(public http: Http) {
    this.url = localStorage.getItem("url")
  }
  
  projects(token:string){
    let url = this.url+"/projects/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  tasks(token:string,project_id:number){
    let url = this.url+"/projects/"+project_id+"/tasks/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  
}
