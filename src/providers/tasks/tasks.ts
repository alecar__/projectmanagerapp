import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';


/*
  Generated class for the TasksProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TasksProvider {
  url:string;
  
  constructor(public http: Http) {
    this.url = localStorage.getItem("url")
  }

  developers(token:string,task_id:number){
    let url = this.url+"/tasks/"+task_id+"/developers/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  comments(token:string,task_id:number){
    let url = this.url+"/tasks/"+task_id+"/comments/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  subtasks(token:string,task_id:number){
    let url = this.url+"/tasks/"+task_id+"/subtasks/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  changestatus(token:string,task_id:number,task_status:number){
    let url = this.url+"/tasks/"+task_id+"/";
    let headers = new Headers({'Authorization': 'JWT '+token, 'Content-Type': 'application/json'});
    let body = JSON.stringify({status: task_status});
    return this.http.patch(url,body,{headers:headers});
  }

  gettaskuser(token:string,task_id:number,user_id:number){
    let url = this.url+"/tasks/"+task_id+"/developers/"+user_id+"/";
    let headers = new Headers({'Authorization': 'JWT '+token});
    return this.http.get(url,{headers:headers});
  }

  changemydata(token:string,task_id:number,user_id:number,mydata:any){
    let url = this.url+"/tasks/"+task_id+"/developers/"+user_id+"/";
    let headers = new Headers({'Authorization': 'JWT '+token, 'Content-Type': 'application/json'});
    let body = JSON.stringify(mydata);
    return this.http.patch(url,body,{headers:headers});
  }


}
