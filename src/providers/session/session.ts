//import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';

@Injectable()
export class SessionProvider {

  url:string;
  
  constructor(public http: Http) {
    this.url = localStorage.getItem("url")
  }
/*
  login(user:string,password:string){ 
    let url = this.url+"token-auth/";
    let body = JSON.stringify({username: user, password: password});
    let headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(url,body,{headers:headers});
  }*/

  login(user:string,password:string){ 
    let url = this.url+"/api-token-auth/";
    let body = JSON.stringify({username: user, password: password});
    let headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post(url,body,{headers:headers});
  }

  currentuser(token:string){
    let url = this.url+"/current_user/";
    let options = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Authorization', 'JWT '+token);
    return this.http.get(url,options);
  }

  /*
  logout(){
    if(localStorage.getItem("token")){

      let email = localStorage.getItem("email").toString();
      let token = localStorage.getItem("token").toString();

      if (email && token) {
        let url = this.url+"sessions/";
        let headers = new Headers({'X-User-Email': email, 'X-User-Token': token});
        return this.http.delete(url,{headers:headers});
      }
    }
  }*/

}
